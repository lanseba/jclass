# jclass
```
java script 类型工具

jclass = javascript class;

   我不知道如何描述她，她没有丰富js组件、也没有实现特定的功能，她不代替其他任何js组件或者框架，她只是一个用于写js面向对象微型框架。
说是一个微型框架都有点过，全部代码只有几十行。

   jclass关注最基本的js代码写法，约束我本人在写js代码时规范，帮助我轻松的驾驭js这门晦涩的语言。
使用jclass可以完全按照面向对象的思路去开发我的功能。使我避免闭包、“类型迷失”带来的麻烦。

   jclass诞生我在几年前一个业务流程系统的项目里，当时需要一个web版的bpmn在线编辑器，而在这之前我其实很害怕写复杂的js代码。
总之在有了她以后再复杂的js模块都能很好规划、组织并开发出来。

没有其他的介绍了。下面就简单写一个用例，看看对你们有没有用。

```

```javascript
//定义一个Human类
JClass("com.tcshuo.Human", function () {
    return {
        //构造函数
        constructor: function (name,age) {
            this.name = name;
            this.age = age;
        }
        , prototype: {
            sayHi : function(){
                if(this._isOld()){
                    alert("我是"+this.name);
                }else{
                    alert("Hi,我叫"+this.name+"我今年"+this.age+"岁,啦啦啦");
                }
            }
            //内部方法使用下滑杠开头
            ,_isOld : function(){
               return this.age>60;
            }
            
        }
    };
});


//Man 继承于Human
JClass("com.tcshuo.Human","com.tcshuo.Man", function (superClass) {
    return {
        constructor: function (name,age) {
           //调用父类构造
           superClass.call(this,name,age);
        }
        , prototype: {
            //内部方法使用下滑杠开头
            _isOld : function(){
               //男人不服老
               return this.age>80;
            }
            
        }
    };
});

//Policeman 继承于Man
JClass("com.tcshuo.Man","com.tcshuo.Policeman", function (superClass) {
    return {
        constructor: function (name,age) {
           //调用父类构造
           superClass.call(this,name,age);
        }
        , prototype: {
             sayHi : function(){
                alert("驾驶证、行车证！");
            }
            
        }
    };
});



//Woman 继承于Human 
JClass("com.tcshuo.Human","com.tcshuo.Woman", function (superClass) {
    return {
        constructor: function (name,age,phrase) {
           //调用父类构造
           superClass.call(this,name,age);
           this.phrase = phrase;
        }
        , prototype: {
            
            sayHi : function(){
                //掉头父类sayHi方法
                superClass.prototype.sayHi.call(this);
                //追加实现
                alert(this.phrase);
            }
            ,_isOld : function(){
              //女人永远不老
               return false;
            }
            
        }
    };
});


//调用一下吧 
var Man = JClass["com.tcshuo.Man"];
var Woman = JClass["com.tcshuo.Woman"];

var nofail = new JClass["com.tcshuo.Human"]("东方不败",100);
var garen = new Man("盖伦",38);
var laozhang = new JClass["com.tcshuo.Policeman"]("老张",50);
var caitlyn = new Woman("凯特琳",26,"你好，召唤师。我是caitlyn");


nofail.sayHi();
garen.sayHi();
laozhang.sayHi();
caitlyn.sayHi();


```