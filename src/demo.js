//定义一个Human类
JClass("com.tcshuo.Human", function () {
    return {
        //构造函数
        constructor: function (name,age) {
            this.name = name;
            this.age = age;
        }
        , prototype: {
            sayHi : function(){
                if(this._isOld()){
                    alert("我是"+this.name);
                }else{
                    alert("Hi,我叫"+this.name+"我今年"+this.age+"岁,啦啦啦");
                }
            }
            //内部方法使用下滑杠开头
            ,_isOld : function(){
               return this.age>60;
            }
            
        }
    };
});


//Man 继承于Human
JClass("com.tcshuo.Human","com.tcshuo.Man", function (superClass) {
    return {
        constructor: function (name,age) {
           //调用父类构造
           superClass.call(this,name,age);
        }
        , prototype: {
            //内部方法使用下滑杠开头
            _isOld : function(){
               //男人不服老
               return this.age>80;
            }
            
        }
    };
});

//Policeman 继承于Man
JClass("com.tcshuo.Man","com.tcshuo.Policeman", function (superClass) {
    return {
        constructor: function (name,age) {
           //调用父类构造
           superClass.call(this,name,age);
        }
        , prototype: {
             sayHi : function(){
                alert("驾驶证、行车证！");
            }
            
        }
    };
});



//Woman 继承于Human 
JClass("com.tcshuo.Human","com.tcshuo.Woman", function (superClass) {
    return {
        constructor: function (name,age,phrase) {
           //调用父类构造
           superClass.call(this,name,age);
           this.phrase = phrase;
        }
        , prototype: {
            
            sayHi : function(){
                //调用父类sayHi方法
                superClass.prototype.sayHi.call(this);
                //追加实现
                alert(this.phrase);
            }
            ,_isOld : function(){
              //女人永远不老
               return false;
            }
            
        }
    };
});


//调用一下吧 
var Man = JClass["com.tcshuo.Man"];
var Woman = JClass["com.tcshuo.Woman"];

var nofail = new JClass["com.tcshuo.Human"]("东方不败",100);
var garen = new Man("盖伦",38);
var laozhang = new JClass["com.tcshuo.Policeman"]("老张",50);
var caitlyn = new Woman("凯特琳",26,"你好，召唤师。我是caitlyn");


nofail.sayHi();
garen.sayHi();
laozhang.sayHi();
caitlyn.sayHi();



