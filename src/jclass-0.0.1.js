(function (_$$_clazz_def_name) {

    /**
     * 定义一个类型 defines 返回参数需包含constructor构造函数与prototype原型
     * 当参数数量为2时 创建类型，当参数数量为3时 继承类型
     * @param {string} superClassName 父类类型名
     * @param {string} className 类型名
     * @param {function} defines 定义函数  返回参数格式 {constructor:function(){},prototype : {} }
     * @returns {JClass} 类型
     */
    var JClass = function (superClassName, className, defines) {

        if (arguments.length < 3) {
            return ExtendClass("jclass.Object", arguments[0], arguments[1]);
        } else {
            return ExtendClass(superClassName, className, defines);
        }

    };

    //内置命名空间实现
    var Namespace = function (name, constructorFunction) {
        var namespace = JClass;
        if (!constructorFunction) {
            return namespace[name];
        }
        namespace[name] = constructorFunction;
        constructorFunction.className = name;
        return constructorFunction;


    }
    /**
     * 内部创建类型实现
     * @param {string} className 类型名
     * @param {function} defines 定义函数  返回参数格式 {constructor:function(){},prototype : {} }
     * @returns {JClass} 类型
     */
    var CreateClass = function (className, defines) {
        var _class = defines();

        //委托构造
        var constructor = function () {
            this.className = this.className || className;
            _class.constructor.apply(this, arguments);
        }
        //原型赋值
        var prototype = _class.prototype;
        constructor.prototype = prototype;
        //命名空间管理
        Namespace(className, constructor)
    }

    /**
     * 内部继承类型实现
     * @param {string} superClassName 父类类型名
     * @param {string} className 类型名
     * @param {function} defines 定义函数  返回参数格式 {constructor:function(){},prototype : {} }
     * @returns {JClass} 类型
     */
    var ExtendClass = function (superClassName, className, defines) {

        var superClass = Namespace(superClassName);
        if (!superClass) {
            throw "未找到类型:" + superClassName;
        }
        //传入父类型,执行定义函数
        var _class = defines(superClass);

        //获取构造函数
        var constructor = _class.constructor;

        //原型
        var prototype = _class.prototype;

        //委派构造
        var constructorFunction = function () {
            this.className = this.className || className;
            constructor.apply(this, arguments);
        }


        //原型覆盖
        var superPrototype = superClass.prototype;
        constructorFunction.prototype = {};

        for (var name in superPrototype) {
            constructorFunction.prototype[name] = superPrototype[name];
        }
        for (var name in prototype) {

            constructorFunction.prototype[name] = prototype[name];
        }
        constructorFunction.superClass = superClass;
        //命名空间管理
        Namespace(className, constructorFunction);
    }

    //定义jclass.Object跟类型
    CreateClass("jclass.Object", function () {
        return {
            constructor: function () {

            }
            , prototype: {
                //获取类型
                getClass: function () {
                    return Namespace(this.className);
                }
                //类型判断
                , isA: function (target) {
                    var targetClass = typeof (target) == "function" ? target : Namespace(target);
                    if (!targetClass) {
                        return false;
                    }
                    var _class = this.getClass();
                    if (_class == targetClass) {
                        return true;
                    }
                    while (_class.superClass) {
                        if (_class.superClass == targetClass) {
                            return true;
                        }
                        _class = _class.superClass;
                    }
                    return false;
                }
            }
        }
    });
    //工具命名 默认为JClass，您可以修改本代码最后一行传入命名参数
    window[_$$_clazz_def_name] = JClass;
}("JClass"));
